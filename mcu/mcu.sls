;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog mcu)
  (export
    mcu?
    make-mcu
    define-mcu

    mcu-name
    mcu-device-code
    mcu-signature
    mcu-parallel-mode
    mcu-polling
    mcu-self-timed
    mcu-lock-bytes
    mcu-fuse-bytes
    mcu-timeout
    mcu-stab-delay
    mcu-cmd-exec-delay
    mcu-sync-loops
    mcu-byte-delay
    mcu-poll-value
    mcu-poll-index
    mcu-flash-memory
    mcu-eeprom-memory)
  (import
    (rnrs)
    (avrprog memory))

(define-record-type mcu
  (fields
    name
    device-code
    signature
    parallel-mode
    polling
    self-timed
    lock-bytes
    fuse-bytes
    timeout
    stab-delay
    cmd-exec-delay
    sync-loops
    byte-delay
    poll-value
    poll-index
    flash-memory
    eeprom-memory))

(define-syntax define-mcu
  (lambda (x)

    (syntax-case x ()
      ((_ var kv ...)
       (with-syntax ([(name0
                       device-code0
                       signature0
                       parallel-mode0
                       polling0
                       self-timed0
                       lock-bytes0
                       fuse-bytes0
                       timeout0
                       stab-delay0
                       cmd-exec-delay0
                       sync-loops0
                       byte-delay0
                       poll-value0
                       poll-index0
                       flash0
                       eeprom0)
                      (generate-temporaries
                     '(name
                       device-code
                       signature
                       parallel-mode
                       polling
                       self-timed
                       lock-bytes
                       fuse-bytes
                       timeout
                       stab-delay
                       cmd-exec-delay
                       sync-loops
                       byte-delay
                       poll-value
                       poll-index
                       flash
                       eeprom))])
         (with-syntax ([(sets ...)
                        (map
                          (lambda (x)
                            (syntax-case x (name
                                            device-code
                                            signature
                                            parallel-mode
                                            polling
                                            self-timed
                                            lock-bytes
                                            fuse-bytes
                                            timeout
                                            stab-delay
                                            cmd-exec-delay
                                            sync-loops
                                            byte-delay
                                            poll-value
                                            poll-index
                                            flash
                                            eeprom)
                              ((name value)
                               #'(set! name0 value))
                              ((device-code value)
                               #'(set! device-code0 value))
                              ((signature value)
                               #'(set! signature0 value))
                              ((parallel-mode value)
                               #'(set! parallel-mode0 value))
                              ((polling value)
                               #'(set! polling0 value))
                              ((self-timed value)
                               #'(set! self-timed0 value))
                              ((lock-bytes value)
                               #'(set! lock-bytes0 value))
                              ((fuse-bytes value)
                               #'(set! fuse-bytes0 value))
                              ((timeout value)
                               #'(set! timeout0 value))
                              ((stab-delay value)
                               #'(set! stab-delay0 value))
                              ((cmd-exec-delay value)
                               #'(set! cmd-exec-delay0 value))
                              ((sync-loops value)
                               #'(set! sync-loops0 value))
                              ((byte-delay value)
                               #'(set! byte-delay0 value))
                              ((poll-value value)
                               #'(set! poll-value0 value))
                              ((poll-index value)
                               #'(set! poll-index0 value))
                              ((flash kv ...)
                               #`(set! flash0 #,(parse-memory-syntax #'make-flash-memory #'(kv ...))))
                              ((eeprom kv ...)
                               #`(set! eeprom0 #,(parse-memory-syntax #'make-eeprom-memory #'(kv ...))))
                            )
                          )
                          #'(kv ...)
                        )])
           #`(define var
               (let ([name0 #f]
                     [device-code0 #f]
                     [signature0 #f]
                     [parallel-mode0 0]
                     [polling0 1]
                     [self-timed0 1]
                     [lock-bytes0 #f]
                     [fuse-bytes0 #f]
                     [timeout0 #f]
                     [stab-delay0 #f]
                     [cmd-exec-delay0 #f]
                     [sync-loops0 #f]
                     [byte-delay0 #f]
                     [poll-value0 #f]
                     [poll-index0 #f]
                     [flash0 #f]
                     [eeprom0 #f])
                 sets ...
                 (make-mcu name0
                           device-code0
                           signature0
                           parallel-mode0
                           polling0
                           self-timed0
                           lock-bytes0
                           fuse-bytes0
                           timeout0
                           stab-delay0
                           cmd-exec-delay0
                           sync-loops0
                           byte-delay0
                           poll-value0
                           poll-index0
                           flash0
                           eeprom0) ))))))))
)
