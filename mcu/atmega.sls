;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog mcu atmega)
  (export
    atmega328p)
  (import
    (rnrs)
    (avrprog mcu))

(define-mcu atmega328p
  (name "ATmega328P")
  (device-code #x86)
  (parallel-mode 1)
  (polling 1)
  (self-timed 1)
  (lock-bytes 1)
  (fuse-bytes 3)
  (timeout 200)
  (stab-delay 100)
  (cmd-exec-delay 25)
  (sync-loops 32)
  (byte-delay 0)
  (poll-index 3)
  (poll-value #x53)
  (signature #vu8(#x1E #x95 #x0F))
  (flash
    (size (* 32 1024))
    (page-size 128)
    (poll-val1 #xff)
    (poll-val2 #xff))
  (eeprom
    (size 1024)
    (poll-val1 #xff)
    (poll-val2 #xff)))

)
