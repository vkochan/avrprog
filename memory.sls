;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog memory)
  (export
    memory?
    make-memory

    memory-size
    memory-page-size
    memory-poll-val1
    memory-poll-val2
    memory-delay

    flash-memory?
    make-flash-memory

    eeprom-memory?
    make-eeprom-memory

    parse-memory-syntax)
  (import
    (rnrs))

(define-record-type memory
  (fields
    size
    page-size
    poll-val1
    poll-val2
    delay))

(define-record-type flash-memory
  (parent memory))

(define-record-type eeprom-memory
  (parent memory))

(define parse-memory-syntax
  (lambda (type x)
    (syntax-case x ()
      ((kv ...)
       (with-syntax ([(size0
                       page-size0
                       poll-val10
                       poll-val20
                       delay0)
                     (generate-temporaries
                     '(size
                       page-size
                       poll-val1
                       poll-val2
                       delay))])
         (with-syntax ([(sets ...)
                        (map
                          (lambda (x)
                            (syntax-case x (size page-size poll-val1 poll-val2 delay)
                              ((size value)
                               #'(set! size0 value))
                              ((page-size value)
                               #'(set! page-size0 value))
                              ((poll-val1 value)
                               #'(set! poll-val10 value))
                              ((poll-val2 value)
                               #'(set! poll-val20 value))
                              ((delay value)
                               #'(set! delay0 value))))
                          #'(kv ...))]) 
           #`(let ((size0 #f)
                   (page-size0 #f)
                   (poll-val10 #f)
                   (poll-val20 #f)
                   (delay0 #f))
               sets ...
               (#,type size0
                       page-size0
                       poll-val10
                       poll-val20
                       delay0))))))))
)
