;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog programmer stk500)
  (export
    stk500v1)
  (import
    (rnrs)
    (avrprog programmer))

(define-programmer stk500v1
  (name "Stk500v1"))

)
