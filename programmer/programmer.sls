;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog programmer)
  (export
    programmer?
    make-programmer

    programmer-name
    programmer-open-func
    programmer-close-func
    programmer-establish-sync-func
    programmer-check-signature-func
    programmer-initialize-device-func
    programmer-enter-program-mode-func
    programmer-leave-program-mode-func
    programmer-program-device-func
    programmer-load-address-func
    programmer-write-page-func
    programmer-read-page-func

    define-programmer)
  (import
    (rnrs))

(define-record-type programmer
  (fields
    name
    open-func
    close-func
    establish-sync-func
    check-signature-func
    initialize-device-func
    enter-program-mode-func
    leave-program-mode-func
    program-device-func
    load-address-func
    write-page-func
    read-page-func))

(define-syntax define-programmer
  (lambda (x)
    (syntax-case x ()
      ((_ var kv ...)
       (with-syntax ([(name0 open0 close0 establish-sync0 check-signature0 initialize-device0
                       enter-program-mode0 leave-program-mode0 program-device0
                       load-address0 write-page0 read-page0)
                      (generate-temporaries
                     '(name open close establish-sync check-signature initialize-device
                       enter-program-mode leave-program-mode program-device
                       load-address write-page read-page))])
         (with-syntax ([(sets ...)
                        (map
                          (lambda (x)
                            (syntax-case x (name open close establish-sync check-signature initialize-device
                                            enter-program-mode leave-program-mode program-device
                                            load-address write-page read-page)
                              ((name value)
                               #'(set! name0 value))
                              ((open value)
                               #'(set! open0 value))
                              ((close value)
                               #'(set! close0 value))
                              ((establish-sync value)
                               #'(set! establish-sync0 value))
                              ((check-signature value)
                               #'(set! check-signature0 value))
                              ((initialize-device value)
                               #'(set! initialize-device0 value))
                              ((enter-program-mode value)
                               #'(set! enter-program-mode0 value))
                              ((leave-program-mode value)
                               #'(set! leave-program-mode0 value))
                              ((program-device value)
                               #'(set! leave-program-mode0 value))
                              ((load-address value)
                               #'(set! load-address0 value))
                              ((write-page value)
                               #'(set! write-page0 value))
                              ((read-page value)
                               #'(set! read-page0 value))
                            )
                          )
                          #'(kv ...)
                        )])
           #`(define var
               (let ([name0 #f]
                     [open0 #f]
                     [close0 #f]
                     [establish-sync0 #f]
                     [check-signature0 #f]
                     [initialize-device0 #f]
                     [enter-program-mode0 #f]
                     [leave-program-mode0 #f]
                     [program-device0 #f]
                     [load-address0 #f]
                     [write-page0 #f]
                     [read-page0 #f])
                 sets ...
                 (make-programmer name0 open0 close0 establish-sync0 check-signature0
                                  initialize-device0 enter-program-mode0 leave-program-mode0
                                  program-device0 load-address0 write-page0 read-page0) ))))))))

)
