;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog)
  (export hello)
  (import (rnrs))

(define (hello whom)
  (string-append "Hello " whom "!")))
