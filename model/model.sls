;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog model)
  (export
    model?
    make-model
    define-model

    model-name
    model-mcu
    model-baudrate
    model-read-timeout
    model-write-timeout
    model-protocol)
  (import
    (rnrs))

(define-record-type model
  (fields name mcu baudrate read-timeout write-timeout protocol))

(define-syntax define-model
  (lambda (x)
    (syntax-case x ()
      ((_ var kv ...)
       (with-syntax ([(name0 mcu0 baudrate0 read-timeout0 write-timeout0 protocol0)
                      (generate-temporaries
                     '(name mcu baudrate read-timwout write-timeout protocol))])
         (with-syntax ([(sets ...)
                        (map
                          (lambda (x)
                            (syntax-case x (name mcu baudrate read-timeout write-timeout protocol)
                              ((name value)
                               #'(set! name0 value))
                              ((mcu value)
                               #'(set! mcu0 value))
                              ((baudrate value)
                               #'(set! baudrate0 value))
                              ((read-timeout value)
                               #'(set! read-timeout0 value))
                              ((write-timeout value)
                               #'(set! write-timeout0 value))
                              ((protocol value)
                               #'(set! protocol0 value))
                            )
                          )
                          #'(kv ...)
                        )])
           #`(define var
               (let ([name0 #f]
                     [mcu0 #f]
                     [baudrate0 #f]
                     [read-timeout0 #f]
                     [write-timeout0 #f]
                     [protocol0 #f])
                 sets ...
                 (make-model name0 mcu0 baudrate0 read-timeout0 write-timeout0 protocol0) ))))))))

)
