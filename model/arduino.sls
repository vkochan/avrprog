;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2024 Vadym Kochan
;; SPDX-License-Identifier: MIT
#!r6rs

(library (avrprog model arduino)
  (export
    arduino-uno-r3)
  (import
    (rnrs)
    (avrprog model)
    (avrprog mcu atmega)
    (avrprog programmer stk500))

(define-model arduino-uno-r3
  (name "Arduino UNO R3")
  (mcu atmega328p)
  (baudrate 115200)
  (read-timeout 1000)
  (write-timeout 1000)
  (protocol stk500v1))
)
